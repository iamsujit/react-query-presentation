/// <reference types="react-scripts" />

/**
 * Primitive types
 */
 type Primitive = string | boolean | number;