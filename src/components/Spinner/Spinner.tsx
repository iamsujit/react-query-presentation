import React from 'react';

const Spinner = () => {
  return (
    <div
      className='spinner-border d-flex justify-content-between align-items-center'
      role='status'
    >
      <span className='sr-only'></span>
    </div>
  );
};

export default Spinner;
