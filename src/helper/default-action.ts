import Axios, { AxiosError, AxiosRequestConfig, AxiosResponse, CancelTokenSource, Method } from 'axios';

import initApiRequest from 'services/api-request';
import { FailToast } from 'components/ToastNotifier/ToastNotifier';
import { APIDetailType } from 'store/apiDetails';
/**
 * Request details for XMLHTTP request
 */
export interface APIRequestDetail {
    /**Request data for the API */
    requestData?: any;
    /** REST API Method
     *
     * This will override requestMethod provided by apiDetails
    */
    requestMethod?: Method;
    /**Path variables present in controller
     * 
     * Provided pathVariables -> {id: 1, type: 'test'}
     * Converts controller-url/{id}/{type} -> controller-url/1/test
    */
    pathVariables?: { [key: string]: Primitive };
    /**Request params
     * 
     * Provided params -> {id: 1, type: 'test'}
     * Converts controller-url -> controller-url?id=1&type=test
    */
    params?: { [key: string]: Primitive | undefined }
    /**Axios cancel token source */
    cancelSource?: CancelTokenSource;
    /**Disable Success Toast */
    disableSuccessToast?: boolean;
    /**Disable Failure Toast */
    disableFailureToast?: boolean;
}

export interface CustomResponse<TData = any> extends AxiosResponse {
    message: string;
    data: TData | null;
    // data: TData | undefined;
    status: number;
    noconnection: boolean;
    config: AxiosRequestConfig;
    isAxiosError: boolean;
}

export type APIResponseDetail<TData = any> = Promise<CustomResponse<TData>>

let timeoutLanguageCount = 0;
let noServerConnectionLanguageCount = 0;
let noConnectionLanguageCount = 0;
const axiosCancelSource = Axios.CancelToken.source();

/**
 * Manages API call and updates reducer with success or failure
 * @param apiDetails redux action and api config
 * @param apiRequestDetails request details for XMLHTTP request
 */
export default async function performApiAction<TData = any>(apiDetails: APIDetailType, apiRequestDetails: APIRequestDetail = {}): Promise<CustomResponse<TData>> {
    const { requestData, requestMethod, pathVariables, params, cancelSource, disableSuccessToast = false, disableFailureToast } = apiRequestDetails;

    // Check for path variables in controllername
    const sanitizedApiDetails = sanitizeController(apiDetails, pathVariables);

    let responseData: any;
    try {
        responseData = await initApiRequest(sanitizedApiDetails, requestData, requestMethod || sanitizedApiDetails.requestMethod || "GET", params, cancelSource || axiosCancelSource);

        // If Backend Error
        // if(responseData.data?.status === 0) {
        //     if (process.env.NODE_ENV === 'production') {
        //     }
        //     throw responseData;
        // }

        if (disableSuccessToast) {
            // No work done
        } else {
            if (![requestMethod, sanitizedApiDetails.requestMethod].includes("GET")) {
                // SuccessToast(responseData.data?.message)
            }
        }

    } catch (customThrownError: any) {
        responseData = customThrownError;
        console.log(responseData);


        if (responseData.status === 401) {
        }

        if (responseData.status === 413) {
            FailToast("कृपया १२ MB भन्दा कम साइजको फाइलहरु अपलोड गर्नुहोला ।")
        }

        if (disableFailureToast) {
            // No work done
        } else {
            responseData?.message && FailToast(responseData?.message);
        }

        // Axios Timeout
        if (responseData.config?.code === 'ECONNABORTED') {
            if (!timeoutLanguageCount) {
                timeoutLanguageCount++;
                // FailToast(requestTimeoutLanguage());
            }
        }

        // No Connection
        if (responseData.noconnection) {
            // No Server Connection
            if (responseData.message === 'Server could not be reached') {
                if (!noServerConnectionLanguageCount) {
                    noServerConnectionLanguageCount++;
                    // FailToast(noConnectionLanguage());
                }
            }
            // No Connection
            else if (responseData.config.code !== 'ECONNABORTED') {
                if (!noConnectionLanguageCount) {
                    noConnectionLanguageCount++;
                    // FailToast(noConnectionLanguage());
                }
            }
        }

        throw new Error(responseData?.message)
    }

    return responseData;
};

function sanitizeController(apiDetail: APIDetailType, pathVariables?: { [key: string]: Primitive }) {
    return pathVariables && Object.keys(pathVariables).length ? {
        ...apiDetail,
        controllerName: Object.entries(pathVariables).reduce((acc, [key, value]) => acc = acc.replace(`{${key}}`, value?.toString()), apiDetail.controllerName)
    }
        :
        apiDetail
}