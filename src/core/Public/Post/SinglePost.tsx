import LoadingButton from 'components/LoadingButton/LoadingButton';
import Spinner from 'components/Spinner/Spinner';
import React from 'react'
import { initialValue } from './index';
import { usePostDataById, usePostByIdDelete, useSavePostByIdHandler } from './posts-query';

export interface PostProps {
  selectedId: string,
  setSelectedId: React.Dispatch<React.SetStateAction<string>>,
  isEdit: boolean,
  setIsEdit: React.Dispatch<React.SetStateAction<boolean>>,
  isAdd: boolean,
  setIsAdd: React.Dispatch<React.SetStateAction<boolean>>
  formValues: typeof initialValue,
  setFormValues: React.Dispatch<React.SetStateAction<typeof initialValue>>
}


const SinglePost = (props: PostProps) => {
  const { isEdit, isAdd, setIsAdd, selectedId, setIsEdit, setFormValues, setSelectedId, formValues } = props;

  const { isLoading: postDataByIdLoading, data: postDataByIdData, isError } = usePostDataById(selectedId)
  // mutation for deleting post
  const deletePostByIdHandler = usePostByIdDelete(selectedId, setSelectedId);
  const savePostByIdHandler = useSavePostByIdHandler({ id: isEdit ? selectedId : Date.now().toString(36), ...formValues }, props);

  const handleInputChange = (event: any) => {
    setFormValues((values) => ({
      ...values,
      [event.target.name]: event.target.value,
    }));
  };

  const submitPost = (e: any) => {
    e.preventDefault();
    if (formValues.body && formValues.title) {
      // savePostByIdHandler.mutate();
      // if optimistic update
      savePostByIdHandler.mutate({ id: isEdit ? selectedId : Date.now().toString(36), ...formValues });
    }
  };

  return (
    <div className="col-md-8">
      <h2>Post Details</h2>
      {!isEdit && !isAdd && (
        <div className='d-flex mb-2 justify-content-end'>
          <button
            onClick={() => setIsAdd(true)}
            className='btn btn-primary btn-sm'
          >
            + Add New Post
          </button>
        </div>
      )}


      <div className='card'>
        <div className='card-body'>
          {!isEdit && !isAdd ? (
            !selectedId ? (
              <p className='card-text'>No Post Selected</p>
            ) : postDataByIdLoading ? (
              <Spinner />
            ) : isError ? (
              <p className='card-text'>Couldn't find the post data</p>
            ) : (
              <>
                <div className=' mb-2 d-flex justify-content-end'>
                  <LoadingButton
                    className='btn btn-sm btn-secondary'
                    onClick={() => {
                      setIsEdit(!isEdit);
                      setFormValues({
                        title: postDataByIdData?.data?.title || "",
                        body: postDataByIdData?.data?.body || "",
                      });
                    }}
                    disabled={deletePostByIdHandler.isLoading}
                  >
                    Edit
                  </LoadingButton>

                  <LoadingButton
                    className='btn btn-sm btn-danger ms-2'
                    onClick={() => {
                      deletePostByIdHandler.mutate();

                      setFormValues(initialValue);
                    }}
                    disabled={deletePostByIdHandler.isLoading}
                    loading={deletePostByIdHandler.isLoading}
                  >
                    Delete
                  </LoadingButton>
                </div>
                <h5 className='card-title'>
                  {postDataByIdData?.data?.title}
                </h5>
                <p className='card-text'>{postDataByIdData?.data?.body}</p>
              </>
            )
          ) : isAdd || isEdit ? (
            <form onSubmit={submitPost}>
              <div className='form-group'>
                <label htmlFor='title'>Title</label>
                <input
                  className='form-control'
                  type='text'
                  name='title'
                  id='title'
                  value={formValues.title}
                  onChange={handleInputChange}
                />
              </div>

              <div className='form-group my-2'>
                <label htmlFor='body'>Body</label>
                <textarea
                  className='form-control'

                  name='body'
                  id='body'
                  value={formValues.body}
                  onChange={handleInputChange}
                  rows={4}

                />
              </div>

              <div>
                <LoadingButton type="submit" className="btn btn-sm btn-success" loading={savePostByIdHandler.isLoading} disabled={savePostByIdHandler.isLoading} text={isEdit ? 'Edit' : 'Add'} />


                <LoadingButton

                  className='btn btn-sm btn-danger ms-2'
                  type='button'
                  onClick={() => {
                    setIsEdit(false);
                    setIsAdd(false);
                    setFormValues(initialValue);
                  }}
                  disabled={savePostByIdHandler.isLoading}
                >
                  Cancel
                </LoadingButton>
              </div>
            </form>
          ) : (
            ''
          )}
        </div>
      </div>
    </div>
  )
}

export default SinglePost
