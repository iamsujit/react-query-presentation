import { useState } from 'react'
import AllPosts from './AllPosts'
import SinglePost from './SinglePost';

export const initialValue = {
  title: '',
  body: '',
};

const Post = () => {
  const [selectedId, setSelectedId] = useState('');
  const [isEdit, setIsEdit] = useState(false);
  const [isAdd, setIsAdd] = useState(false);
  const [formValues, setFormValues] = useState(initialValue)

  return (
    <div className="row">
      <AllPosts selectedId={selectedId} setSelectedId={setSelectedId} isEdit={isEdit} setIsEdit={setIsEdit} isAdd={isAdd} setIsAdd={setIsAdd} formValues={formValues} setFormValues={setFormValues} />
      <SinglePost selectedId={selectedId} setSelectedId={setSelectedId} isEdit={isEdit} setIsEdit={setIsEdit} isAdd={isAdd} setIsAdd={setIsAdd} formValues={formValues} setFormValues={setFormValues} />
    </div>
  )
}

export default Post
