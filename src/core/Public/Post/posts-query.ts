import performApiAction from 'helper/default-action';
import { useQuery, useMutation, useQueryClient, } from 'react-query';
import { apiList } from 'store/apiDetails';
import { PostProps } from './SinglePost';
import { initialValue } from './index';


const { getAllPost, getPostDetailsById, deletePostById, editPostById, addNewPost } = apiList.posts

interface PostDataResponse {
  id: string, title: string, body: string
}

export const usePostData = () => useQuery(getAllPost.queryKeyName, () => performApiAction<PostDataResponse[]>(getAllPost));

// Dependent Query 
export const usePostDataById = (postDataId: string) => {
  return useQuery(
    [getPostDetailsById.queryKeyName, postDataId],
    () =>
      performApiAction<PostDataResponse>(getPostDetailsById, {
        pathVariables: { id: postDataId },
      }),
    {
      // The query will not execute until the postDataId exists
      enabled: !!postDataId,
    }
  );
};

//mutations are typically used to create/update/delete data or perform server side-effects. 


// mutation for add and editing post
export const useSavePostByIdHandler = (requestData: PostDataResponse, otherParams: PostProps) => {
  const { isEdit, isAdd, setSelectedId, setIsAdd, setIsEdit, setFormValues, selectedId } = otherParams
  const queryClient = useQueryClient()

  return useMutation(
    () =>
      performApiAction<PostDataResponse>(isEdit ? editPostById : addNewPost, {
        requestData,
        pathVariables: { id: selectedId },
      }),
    {
      onMutate: async (variables: PostDataResponse) => {
        //for optimistic updates

        // Cancel any outgoing refetches (so they don't overwrite our optimistic update)
        // await queryClient.cancelQueries(getAllPost.queryKeyName)
        // await queryClient.cancelQueries([getPostDetailsById.queryKeyName, selectedId])

        // // Snapshot the previous value
        // const previousPosts = queryClient.getQueryData(getAllPost.queryKeyName)

        // // Optimistically update to the new value
        // queryClient.setQueryData(getAllPost.queryKeyName, (old: any) => {
        //   return {
        //     ...old,
        //     data: [...old.data, variables]
        //   }

        // })

        // setIsAdd(false);
        // setFormValues(initialValue);

        // // Return a context object with the snapshotted value
        // return { previousPosts }

      },
      onSuccess: (data: any, variable, context) => {
        if (!data?.data?.status) {
          setFormValues(initialValue);
          isEdit &&
            queryClient.invalidateQueries([getPostDetailsById.queryKeyName, selectedId]); //invalidating cache query and refetching updated post id
          isAdd && setSelectedId('');
          setIsEdit(false);
          setIsAdd(false);
          queryClient.invalidateQueries([getAllPost.queryKeyName]); // invalidating cache query and refetching all post
        }
      },
      // If the mutation fails, use the context returned from onMutate to roll back
      onError: (err, postData, context) => {


        // queryClient.setQueryData(getAllPost.queryKeyName, context?.previousPosts)
      },
      // Always refetch after error or success:
      onSettled: () => {
        // queryClient.invalidateQueries([getAllPost.queryKeyName]);
      },
    }
  )
}

// mutation for deleting post
export const usePostByIdDelete = (deleteId: string, setSelectedId: any) => {

  const queryClient = useQueryClient()

  return useMutation(
    () =>
      performApiAction(deletePostById, {
        pathVariables: { id: deleteId },
      }),
    {
      onSuccess: (data: any) => {
        if (!data?.data?.status) {

          queryClient.invalidateQueries([getAllPost.queryKeyName]); // invalidating cache query and refetching all post
          setSelectedId('');
        }
      },
    }
  )
}
