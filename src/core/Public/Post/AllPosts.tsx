import Spinner from 'components/Spinner/Spinner';
import { usePostData } from './posts-query';
import { PostProps } from './SinglePost';

const AllPosts = (props: PostProps) => {
  const { setIsAdd, setIsEdit, setSelectedId } = props;
  const { isLoading, data: postData, isError } = usePostData()


  // if (isLoading) {
  //   return <Spinner />
  // }

  // if (isError) {
  //   return <span>Error finding post data</span>
  // }

  return <div className='col-md-4 '>
    <h2>Post List</h2>

    {isLoading ? <Spinner /> : isError ? <span>Error finding post data</span> : postData?.data?.length ? postData?.data?.map((post) => (
      <div
        className='card'
        key={post.id}
        style={{ cursor: 'pointer' }}
        onClick={() => {
          setSelectedId(post.id);
          setIsEdit(false);
          setIsAdd(false);
        }}
      >
        <div className='card-body'>
          <h5 className='card-title'>{post.title}</h5>
          <p className='card-text'>{post.body}</p>
        </div>
      </div>
    )) : (
      'No Post found'
    )}

  </div>

}

export default AllPosts
