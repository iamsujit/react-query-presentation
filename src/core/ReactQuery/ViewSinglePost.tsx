import axios from 'axios';
import Spinner from 'components/Spinner/Spinner';
import React from 'react'
import { useQuery } from 'react-query';
import { initialValue } from './index';

export interface PostProps {
  selectedId: string,
  setSelectedId: React.Dispatch<React.SetStateAction<string>>,
  isEdit: boolean,
  setIsEdit: React.Dispatch<React.SetStateAction<boolean>>,
  isAdd: boolean,
  setIsAdd: React.Dispatch<React.SetStateAction<boolean>>
  formValues: typeof initialValue,
  setFormValues: React.Dispatch<React.SetStateAction<typeof initialValue>>
}


const ViewSinglePost = (props: PostProps) => {
  const { selectedId } = props;

  const { isLoading: postDataByIdLoading, data: postDataByIdData, isError } = useQuery(
    ["GET_POST_DATA_BY_ID", selectedId],
    async () => {
      const getPostByIdRes = await axios.get(`http://localhost:3000/posts/${selectedId}`)
      return getPostByIdRes.data
    },

    {
      // The query will not execute until the selectedId exists
      enabled: !!selectedId,
    }
  );




  return (
    <div className="col-md-8">
      <h2>Post Details</h2>



      <div className='card'>
        <div className='card-body'>
          {!selectedId ? (
            <p className='card-text'>No Post Selected</p>
          ) : postDataByIdLoading ? (
            <Spinner />
          ) : isError ? (
            <p className='card-text'>Couldn't find the post data</p>
          ) : (
            <>

              <h5 className='card-title'>
                {postDataByIdData?.title}
              </h5>
              <p className='card-text'>{postDataByIdData?.body}</p>
            </>
          )
          }
        </div>
      </div>
    </div>
  )
}

export default ViewSinglePost
