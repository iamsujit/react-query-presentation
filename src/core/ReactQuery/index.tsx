import { useState } from 'react'
import QueryAllPosts from './QueryAllPosts'
import QuerySinglePost from './QuerySinglePost';
import ViewSinglePost from './ViewSinglePost';


export const initialValue = {
  title: '',
  body: '',
};

const QueryPost = () => {
  const [selectedId, setSelectedId] = useState('');
  const [isEdit, setIsEdit] = useState(false);
  const [isAdd, setIsAdd] = useState(false);
  const [formValues, setFormValues] = useState(initialValue)

  return (
    <div className="row">
      <QueryAllPosts selectedId={selectedId} setSelectedId={setSelectedId} isEdit={isEdit} setIsEdit={setIsEdit} isAdd={isAdd} setIsAdd={setIsAdd} formValues={formValues} setFormValues={setFormValues} />
      <QuerySinglePost selectedId={selectedId} setSelectedId={setSelectedId} isEdit={isEdit} setIsEdit={setIsEdit} isAdd={isAdd} setIsAdd={setIsAdd} formValues={formValues} setFormValues={setFormValues} />
    </div>
  )
}

export default QueryPost
