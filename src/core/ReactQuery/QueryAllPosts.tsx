import Spinner from 'components/Spinner/Spinner';
import { PostProps } from './QuerySinglePost';
import { useQuery } from 'react-query';
import axios from 'axios';

export interface PostResponseData {
  id: string,
  title: string,
  body: string
}

const AllPosts = (props: PostProps) => {
  const { setIsAdd, setIsEdit, setSelectedId } = props;

  const { data: postData, isError, isLoading } = useQuery(['GET_ALL_POST_DATA'],
    async (): Promise<PostResponseData[]> => {
      const getAllPostRes = await axios.get("http://localhost:3000/posts")
      return getAllPostRes.data
    },

    {

    })




  if (isLoading) {
    return <Spinner />
  }

  if (isError) {
    return <span>Error finding post data</span>
  }


  return <div className='col-md-4 '>
    <h2>Post List</h2>

    {isLoading ? <Spinner /> : isError ? <span>Error finding post data</span> : postData?.length ? postData?.map((post) => (
      <div
        className='card'
        key={post.id}
        style={{ cursor: 'pointer' }}
        onClick={() => {
          setSelectedId(post.id);
          setIsEdit(false);
          setIsAdd(false);
        }}
      >
        <div className='card-body'>
          <h5 className='card-title'>{post.title}</h5>
        </div>
      </div>
    )) : (
      'No Post found'
    )}

  </div>

}

export default AllPosts
