import LoadingButton from 'components/LoadingButton/LoadingButton';
import Spinner from 'components/Spinner/Spinner';
import React from 'react'
import { initialValue } from './index';
import axios from 'axios';
import { useQuery, useMutation, useQueryClient } from 'react-query';
// import { PostResponseData } from './QueryAllPosts';

export interface PostProps {
  selectedId: string,
  setSelectedId: React.Dispatch<React.SetStateAction<string>>,
  isEdit: boolean,
  setIsEdit: React.Dispatch<React.SetStateAction<boolean>>,
  isAdd: boolean,
  setIsAdd: React.Dispatch<React.SetStateAction<boolean>>
  formValues: typeof initialValue,
  setFormValues: React.Dispatch<React.SetStateAction<typeof initialValue>>
}


const SinglePost = (props: PostProps) => {
  const { isEdit, isAdd, setIsAdd, selectedId, setIsEdit, setFormValues, setSelectedId, formValues } = props;

  const queryClient = useQueryClient()

  const { isLoading: postDataByIdLoading, data: postDataByIdData, isError } = useQuery(
    ["GET_POST_DATA_BY_ID", selectedId],
    async () => {
      const getPostByIdRes = await axios.get(`http://localhost:3000/posts/${selectedId}`)
      return getPostByIdRes.data
    },

    {
      // The query will not execute until the selectedId exists
      enabled: !!selectedId,
    }
  );

  const { mutate: savePost, isLoading: savePostFetching } = useMutation(() => axios.post('http://localhost:3000/posts', {
    ...formValues,
    id: Date.now().toString(36)
  }),
    {
      onMutate: (variables) => {
        // return variables;
      },
      onSuccess: (data) => {
        queryClient.invalidateQueries(["GET_ALL_POST_DATA"])
        setFormValues(initialValue);
        setIsAdd(false)
      },

    },

  );





  // mutation for deleting post
  const { mutate: deletePost, isLoading: deletePostFetching } = useMutation(() => axios.delete(`http://localhost:3000/posts/${selectedId}`),
    {

      onSuccess: () => {
        queryClient.invalidateQueries(["GET_ALL_POST_DATA"])
        setSelectedId("")
      },

    },

  )

  const handleInputChange = (event: any) => {
    setFormValues((values) => ({
      ...values,
      [event.target.name]: event.target.value,
    }));
  };

  const submitPost = (e: any) => {
    e.preventDefault();
    if (formValues.body && formValues.title) {
      savePost();
      // if optimistic update
      // savePost({ id: isEdit ? selectedId : Date.now().toString(36), ...formValues });
    }
  };


  return (
    <div className="col-md-8">
      <h2>Post Details</h2>
      {!isEdit && !isAdd && (
        <div className='d-flex mb-2 justify-content-end'>
          <button
            onClick={() => setIsAdd(true)}
            className='btn btn-primary btn-sm'
          >
            + Add New Post
          </button>
        </div>
      )}


      <div className='card'>
        <div className='card-body'>
          {!isEdit && !isAdd ? (
            !selectedId ? (
              <p className='card-text'>No Post Selected</p>
            ) : postDataByIdLoading ? (
              <Spinner />
            ) : isError ? (
              <p className='card-text'>Couldn't find the post data</p>
            ) : (
              <>
                <div className=' mb-2 d-flex justify-content-end'>
                  {/* <LoadingButton
                    className='btn btn-sm btn-secondary'
                    onClick={() => {
                      setIsEdit(!isEdit);
                      setFormValues({
                        title: postDataByIdData?.title || "",
                        body: postDataByIdData?.body || "",
                      });
                    }}
                  disabled={deletePostByIdHandler.isLoading}
                  >
                    Edit
                  </LoadingButton> */}

                  <LoadingButton
                    className='btn btn-sm btn-danger ms-2'
                    onClick={() => {
                      deletePost();
                      setFormValues(initialValue);
                    }}
                    disabled={deletePostFetching}
                    loading={deletePostFetching}
                  >
                    Delete
                  </LoadingButton>
                </div>
                <h5 className='card-title'>
                  {postDataByIdData?.title}
                </h5>
                <p className='card-text'>{postDataByIdData?.body}</p>
              </>
            )
          ) : isAdd || isEdit ? (
            <form onSubmit={submitPost}>
              <div className='form-group'>
                <label htmlFor='title'>Title</label>
                <input
                  className='form-control'
                  type='text'
                  name='title'
                  id='title'
                  value={formValues.title}
                  onChange={handleInputChange}
                />
              </div>

              <div className='form-group my-2'>
                <label htmlFor='body'>Body</label>
                <textarea
                  className='form-control'

                  name='body'
                  id='body'
                  value={formValues.body}
                  onChange={handleInputChange}
                  rows={4}

                />
              </div>

              <div>
                <LoadingButton
                  type="submit"
                  className="btn btn-sm btn-success"
                  loading={savePostFetching}
                  disabled={savePostFetching}
                  text={isEdit ? 'Edit' : 'Add'}
                />


                <LoadingButton

                  className='btn btn-sm btn-danger ms-2'
                  type='button'
                  onClick={() => {
                    setIsEdit(false);
                    setIsAdd(false);
                    setFormValues(initialValue);
                  }}
                  disabled={savePostFetching}
                >
                  Cancel
                </LoadingButton>
              </div>
            </form>
          ) : (
            ''
          )}
        </div>
      </div>
    </div>
  )
}

export default SinglePost
